//
//  SignupViewController.swift
//  student570001
//
//  Created by Erik Alserda on 04/11/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

class SignupViewController:  UIViewController {

    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordConfirmationTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("signup", comment: "")
    }
    
    @IBAction func signup(_ sender: Any) {
        guard let userName = userNameTextField.text, userName.count > 0 else {
            errorLabel.text = NSLocalizedString("no_username_warning", comment: "")
            return
        }
        
        guard let password = passwordTextField.text, password.count > 0 else {
            errorLabel.text = NSLocalizedString("no_password_warning", comment: "")
            return
        }
        
        guard let passwordConfirmation = passwordConfirmationTextField.text, passwordConfirmation.count > 0 else {
            errorLabel.text = NSLocalizedString("no_repeat_password_warning", comment: "")
            return
        }
        
        guard password == passwordConfirmation else {
            errorLabel.text = NSLocalizedString("repeat_password_wrong", comment: "")
            return
        }
        errorLabel.text = ""
        
        let user = User(userName: userName, password: password)
        NewsreaderAPIService.signUp(user: user)
            .responseData(completionHandler: { [weak self] (response) in
                guard let jsonData = response.data else { return}
                guard let registerResponse = try? JSONDecoder().decode(User.RegisterResponse.self, from: jsonData) else { return }
                
                if(registerResponse.success) {
                    NewsreaderAPIService.login(user: user)
                        .responseData(completionHandler: { [weak self] (response) in
                            guard let jsonData = response.data else { return}
                            guard let loginResponse = try? JSONDecoder().decode(User.LoginResponse.self, from: jsonData) else { return }
                            
                            if let authToken = loginResponse.authToken {
                                KeychainWrapper.standard.set(authToken, forKey: "authToken")
                                let newsreaderVc = self?.mainStoryboard.instantiateViewController(withIdentifier:
                                    "newsreaderViewController") as! ViewController
                                self?.navigationController?.pushViewController(newsreaderVc, animated: true)
                            }
                        })
                } else {
                    DispatchQueue.main.async {
                        self?.errorLabel.text = registerResponse.message
                    }
                }
            })
        
    }
    
    func loginUser() {
        
    }
}

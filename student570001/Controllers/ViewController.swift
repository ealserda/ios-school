//
//  ViewController.swift
//  student570001
//
//  Created by Erik Alserda on 30/10/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import UIKit
import Kingfisher
import Alamofire

class ViewController: UITableViewController {
    
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let nc = NotificationCenter.default
    var favorites = false
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    var networkrequestPending = false {
        didSet {
            activityIndicatorView.isHidden = !networkrequestPending
        }
    }
    var nextId: Int?
    var articles = [Article]() {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(favorites && articles.count == 0) {
            self.fetchArticles()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.favorites = self.navigationController?.restorationIdentifier == "FavoriteNavigationController"
        if(self.favorites) {
            self.title = NSLocalizedString("favorites", comment: "")
        } else {
            self.title = NSLocalizedString("application_title", comment: "")
            setRightBarButton()
        }
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.hidesBackButton = true
        
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            tabArray[1].isEnabled = AuthService.isloggedIn()
        }
        
        tableView.separatorColor = UIColor.clear
        nc.addObserver(self, selector: #selector(userLoggedOut), name: Notification.Name("UserLoggedOut"), object: nil)
        nc.addObserver(self, selector: #selector(articleLikeChanged(_:)), name: Notification.Name("ArticleLikeChanged"), object: nil)
        
        fetchArticles()
    }
    
    @objc func articleLikeChanged(_ notification: Notification) {
        if(self.favorites) {
            fetchArticles()
        } else {
            if let articleData = notification.userInfo as? [Int: Bool] {
                if let data = articleData.first {
                    if let index = articles.index(where: { $0.id == data.key }) {
                        articles[index].isLiked = data.value
                    }
                }
            }
        }
    }
    
    func setRightBarButton () {
        if(AuthService.isloggedIn()) {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("logout", comment: ""), style: .plain, target: self, action: #selector(logoutButtonClicked))
        } else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("login", comment: ""), style: .plain, target: self, action: #selector(loginButtonClicked))
        }
    }
    
    @objc func userLoggedOut () {
        articles = [Article]()
        fetchArticles(whenDone: {() in
        })
        if(self.favorites) {
            self.navigateToNewsreader()
        } else {
            self.setRightBarButton()
        }
        
        let tabBarControllerItems = self.tabBarController?.tabBar.items
        
        if let tabArray = tabBarControllerItems {
            tabArray[1].isEnabled = AuthService.isloggedIn()
        }
    }
    
    @objc func loginButtonClicked () {
        let loginVc = mainStoryboard.instantiateViewController(withIdentifier:
            "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(loginVc, animated: true)
    }
    
    func navigateToNewsreader () {
        self.tabBarController?.selectedIndex = 0
    }
    
    @objc func logoutButtonClicked () {
        AuthService.logout()
    }
    
    func fetchArticles (nextId: Int? = nil, whenDone done: (()->())? = nil) {
        networkrequestPending = true
        
        if(self.favorites) {
            guard AuthService.isloggedIn() else { navigateToNewsreader(); return }
            NewsreaderAPIService.getFavoriteAricles()
                .responseData(completionHandler: { [weak self] (response) in
                    guard let jsonData = response.data else { return }
                    guard let favoriteArticlesResponse = try? JSONDecoder().decode(ArticlesEnvelope.self, from: jsonData) else { return }
                    
                    self?.articles = favoriteArticlesResponse.articles
                    finishRequest()
                })
        } else {
            NewsreaderAPIService.getArticles(nextId: nextId, articlesCount: 20)
                .responseData(completionHandler: { [weak self] (response) in
                    guard let jsonData = response.data else {
                        finishRequest()
                        return
                    }
                    
                    guard let newsArticlesResponse = try? JSONDecoder().decode(ArticlesEnvelope.self, from: jsonData) else {
                        finishRequest()
                        return
                    }
                    
                    if let _ = nextId {
                        self?.articles += newsArticlesResponse.articles
                    } else {
                        self?.articles = newsArticlesResponse.articles
                    }
                    self?.nextId = newsArticlesResponse.nextId
                    finishRequest()
                })
        }
        
        func finishRequest () {
            self.networkrequestPending = false
            done?()
        }
    }
    
    @IBAction func refresh(_ sender: UIRefreshControl) {
        fetchArticles(whenDone: {() in
            sender.endRefreshing()
        })
    }
    
    
}


extension ViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ArticleCell", for: indexPath) as! CustomTableViewCell
        
        let article : Article = articles[indexPath.row]
        cell.article = article

        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let totalUsers = articles.count
        
        guard !self.favorites else { return }
        guard indexPath.row >= totalUsers - 3 else { return }
        guard networkrequestPending == false else { return }
        if let id = nextId {
            fetchArticles(nextId: id)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let articleDetailVc = mainStoryboard.instantiateViewController(withIdentifier:
            "DetailViewController") as! DetailViewController
        let article : Article = articles[indexPath.row]
        
        articleDetailVc.article = article
        
        self.navigationController?.pushViewController(articleDetailVc, animated: true)
    }
}

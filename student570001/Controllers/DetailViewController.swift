//
//  DetailViewController.swift
//  student570001
//
//  Created by Erik Alserda on 03/11/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import Foundation
import UIKit

class DetailViewController:  UIViewController {
    
    let nc = NotificationCenter.default
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var likeSwitch: UISwitch!
    @IBOutlet weak var ralatedLabel: UILabel!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articleSummaryLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    let imagePlaceholder = UIImage(named: "placeholder")
    
    var article: Article?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("share", comment: ""), style: .plain, target: self, action: #selector(shareButtonClicked))
        
        articleTitleLabel.text = article?.title
        articleSummaryLabel.text = article?.summary
        if let urlString = article?.image {
            let url = URL(string: urlString)
            articleImageView.kf.setImage(with: url, placeholder: imagePlaceholder, options: [.transition(.fade(0.2))])
        }
        
        if let relatedLinks = article?.related {
            for relatedLink in relatedLinks {
                ralatedLabel.text? += relatedLink + "\n"
            }
        }
        
        if let cateogories = article?.categories {
            for cateogory in cateogories {
                categoryLabel.text? +=  "\(cateogory.name), "
            }
        }
        
        if let publishDate = article?.publishDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            if let date = dateFormatter.date(from: publishDate) {
                dateFormatter.dateFormat = "EEEE d MMMM yyyy HH:mm"
                self.dateLabel.text = dateFormatter.string(from: date)
            }
            
            
        }
        
        if let liked = article?.isLiked, liked {
            likeSwitch.setOn(true, animated: true)
        }
        
        if(AuthService.isloggedIn()){
            likeSwitch.isEnabled = true
        }
        
    }
    
    @objc func shareButtonClicked() {
        if let articleUrl = article?.url {
            if let url = NSURL(string: articleUrl) {
                let objectsToShare = [NSLocalizedString("share_text", comment: ""), url] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                
                self.present(activityVC, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func readMore(_ sender: Any) {
        if let articleUrl = article?.url {
            if let url = URL(string: articleUrl) {
                UIApplication.shared.open(url)
            }
        }
    }
    
    @IBAction func likeArticle(_ sender: Any) {
        NewsreaderAPIService.toggleLikeArticle(article: article!)
            .responseData(completionHandler: { [weak self] (response) in
                if let isLiked = self?.article!.isLiked {
                    self?.article!.isLiked = !isLiked
                    let articleData = [self?.article!.id: self?.article!.isLiked]
                    self?.nc.post(name: NSNotification.Name(rawValue: "ArticleLikeChanged"), object: nil, userInfo: articleData)
                }
        })
    }
    
    
    
}

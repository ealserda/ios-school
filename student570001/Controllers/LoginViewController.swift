//
//  LoginViewController.swift
//  student570001
//
//  Created by Erik Alserda on 03/11/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

class LoginViewController:  UIViewController {

    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("login", comment: "")
    }
    
    @IBAction func login(_ sender: Any) {
        
        guard let userName = userNameTextField.text else {return}
        guard let password = passwordTextField.text else {return}
        
        let user = User(userName: userName, password: password)
        
        NewsreaderAPIService.login(user: user)
            .responseData(completionHandler: { [weak self] (response) in
                guard let jsonData = response.data else { return}
                guard let loginResponse = try? JSONDecoder().decode(User.LoginResponse.self, from: jsonData) else { return }
                
                if let authToken = loginResponse.authToken {
                    KeychainWrapper.standard.set(authToken, forKey: "authToken")
                    let newsreaderVc = self?.mainStoryboard.instantiateViewController(withIdentifier:
                        "newsreaderViewController") as! ViewController
                    self?.navigationController?.pushViewController(newsreaderVc, animated: false)
                }
            })
    }
    
    @IBAction func signup(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let signupVc = storyboard.instantiateViewController(withIdentifier:
            "SignupViewController") as! SignupViewController
        self.navigationController?.pushViewController(signupVc, animated: true)
    }
    
}

//
//  Response.swift
//  student570001
//
//  Created by Erik Alserda on 30/10/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import Foundation

struct ArticlesEnvelope: Codable {
    let articles: [Article]
    let nextId: Int
    
    enum CodingKeys: String, CodingKey {
        case articles = "Results"
        case nextId = "NextId"
    }
}

//
//  User.swift
//  student570001
//
//  Created by Erik Alserda on 04/11/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import Foundation

class User {
    var userName: String
    var password: String
    
    init (userName: String, password: String) {
        self.userName = userName
        self.password = password
    }
    
    struct LoginResponse: Codable {
        let authToken: String?
        
        enum CodingKeys: String, CodingKey {
            case authToken = "AuthToken"
        }
    }
    
    struct RegisterResponse: Codable {
        let success: Bool
        let message: String
        
        enum CodingKeys: String, CodingKey {
            case success = "Success"
            case message = "Message"
        }
    }
}


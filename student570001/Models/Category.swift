//
//  Category.swift
//  student570001
//
//  Created by Erik Alserda on 09/11/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import Foundation

struct Category: Codable {
    let id: Int
    let name: String
    
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
    }
}

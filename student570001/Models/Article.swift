//
//  Article.swift
//  student570001
//
//  Created by Erik Alserda on 30/10/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import Foundation


struct Article: Codable {
    let id: Int
    let feed: Int
    let title: String
    let summary: String
    let publishDate: String
    let image: String
    let url: String
    let related: [String]
    let categories: [Category]
    var isLiked: Bool
    
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case feed = "Feed"
        case title = "Title"
        case summary = "Summary"
        case publishDate = "PublishDate"
        case image = "Image"
        case url = "Url"
        case related = "Related"
        case categories = "Categories"
        case isLiked = "IsLiked"
    }
}

//
//  AuthService.swift
//  student570001
//
//  Created by Erik Alserda on 04/11/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

final class AuthService {
    
    static func getAuthToken() -> String? {
        return KeychainWrapper.standard.string(forKey: "authToken")
    }
    
    static func isloggedIn() -> Bool {
        guard let authToken = getAuthToken() else { return false }
        guard authToken.count > 0 else { return false }
        return true
    }
    
    static func logout() {
        KeychainWrapper.standard.removeObject(forKey: "authToken")
        NotificationCenter.default.post(name: Notification.Name("UserLoggedOut"), object: nil)
    }
 
}

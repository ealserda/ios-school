//
//  NewsreaderAPI.swift
//  student570001
//
//  Created by Erik Alserda on 03/11/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import Foundation
import Alamofire
import SwiftKeychainWrapper

final class NewsreaderAPIService {
    static let baseURL = "https://inhollandbackend.azurewebsites.net/api/"
    
    static func getArticles(nextId: Int? = nil, articlesCount: Int = 20) -> DataRequest {

        var headers: [String: String] = [:]
        if let authToken = AuthService.getAuthToken() {
            headers["x-authtoken"] = authToken
        }
        
        var parameters: [String: String] = [:]
        parameters["count"] = "\(articlesCount)"
        
        if let nextId = nextId {
            return Alamofire.request(baseURL + "Articles/\(nextId)", method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
        } else {
            return Alamofire.request(baseURL + "Articles", method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
        }
    }
    
    static func getFavoriteAricles() -> DataRequest {
        var headers: [String: String] = [:]
        if let authToken = AuthService.getAuthToken() {
            headers["x-authtoken"] = authToken
        }
        return Alamofire.request(baseURL + "Articles/liked", method: .get, encoding: URLEncoding.default, headers: headers)
    }
    
    static func login(user: User) -> DataRequest {
        
        var parameters: [String: String] = [:]
        parameters["UserName"] = "\(user.userName)"
        parameters["Password"] = "\(user.password)"
        
        return Alamofire.request(baseURL + "Users/login", method: .post, parameters: parameters)
    }
    
    static func signUp(user: User) -> DataRequest {
        var parameters: [String: String] = [:]
        parameters["UserName"] = "\(user.userName)"
        parameters["Password"] = "\(user.password)"
        
        return Alamofire.request(baseURL + "Users/register", method: .post, parameters: parameters)
    }
    
    static func toggleLikeArticle(article: Article) -> DataRequest {
        
        var headers: [String: String] = [:]
        if let authToken = AuthService.getAuthToken() {
            headers["x-authtoken"] = authToken
        }
        
        return Alamofire.request(baseURL + "Articles/\(article.id)/like", method: article.isLiked ? .delete : .put, encoding: URLEncoding.default, headers: headers)
    }
    
}

//
//  CustomTableViewCell.swift
//  student570001
//
//  Created by Erik Alserda on 30/10/2018.
//  Copyright © 2018 Erik Alserda. All rights reserved.
//

import Foundation
import UIKit

final class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var favImageView: UIImageView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    let imagePlaceholder = UIImage(named: "placeholder")
    
    var article: Article? {
        didSet {
            newsTitleLabel.text = article?.title
            
            if let urlString = article?.image {
                let url = URL(string: urlString)
                newsImageView.kf.setImage(with: url, placeholder: imagePlaceholder, options: [.transition(.fade(0.2))])
                
                if let isLiked = self.article?.isLiked {
                    if (isLiked) {
                        self.favImageView.image = UIImage(named: "heartred")
                    }
                }
                UIViewPropertyAnimator(duration: 0.4, curve: .easeIn) {
                    self.newsImageView.layer.cornerRadius = 20
                }.startAnimation()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.newsImageView.layer.borderColor = UIColor.red.cgColor
        
        UIViewPropertyAnimator(duration: 0.4, curve: .easeIn) {
            self.newsImageView.layer.cornerRadius = 20
        }.startAnimation()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        newsTitleLabel.text = nil
        newsImageView.image = nil
        favImageView.image = UIImage(named: "heartgray")
        newsImageView.layer.borderWidth = 0
    }

}
